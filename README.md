# This is my linux home setup.

## Apps

I have migrated to the `task` executable to install all of the tools I need.

1. Create the directories that will be needed.
    ```bash
    mkdir -p ${HOME}/.local/{bin,completion,include,lib,share}
    ```

1. Install `task` into `${HOME}/.local/bin/`

    ```bash
    mkdir temp
    cd temp
    curl -sL https://github.com/go-task/task/releases/download/v3.31.0/task_linux_amd64.tar.gz | tar xz
    mv task ${HOME}/.local/bin/task
    cd ..
    rm -rf temp/
    ```

1. Run `task` to install everything else.

    ```bash
    cd tasks/
    task
    ```
