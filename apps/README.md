# APPs README

> **_Note:_**
>
> This is meant to be an empty folder, where I can store any applicaitons that
> are spcific to the user/configuration of the machine this is installed on.
>
> For any program you want accessable to this user, create a link or move into ``./bin``.

- ### TMUX

    ``` bash
    cd temp
    curl -LO https://github.com/tmux/tmux/releases/download/3.1b/tmux-3.1b-x86_64.AppImage
    chmod +x tmux*.appimage
    ./tmux*.appimage --appimage-extract
    cd squashfs-root/usr/bin
    mv tmux $APPS_DIR/bin/tmux
    rm -rf ./*
    ```

- ### NeoVim


    ``` bash
    cd temp
    curl -LO https://github.com/neovim/neovim/releases/download/v0.7.0/nvim.appimage
    chmod +x nvim.appimage
    ./nvim.appimage --appimage-extract
    mv squashfs-root/usr/bin/nvim ../bin/nvim
    sudo dnf install -y python39
    python3 -m pip install --user wheel
    python3 -m pip install --user neovim
    git clone --depth 1 https://github.com/wbthomason/packer.nvim \
    ~/.local/share/nvim/site/pack/packer/start/packer.nvim
    rm -rf ./*
    vi
    ```

    ```
    :PackerInstall
    ```

- ### Fuzzy Finder (In go-task)

    ``` bash
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install
    ```

- ### BAT

    Updated `cat`

    ``` bash
    cd temp
    wget https://github.com/sharkdp/bat/releases/download/v0.16.0/bat-v0.16.0-i686-unknown-linux-musl.tar.gz
    tar xf bat*
    mv bat<tab>/bat ../bin/bat
    rm *
    ```

- ### Lab (GitLab CLI)

    ``` bash
    cd temp
    wget https://github.com/zaquestion/lab/releases/download/v0.23.0/lab_0.23.0_linux_amd64.tar.gz
    tar xf lab*
    mv lab ../bin/lab
    rm *
    ```

- ### OKD/OpenShift - oc

    > **_Note:_**
    > Go to:
    > https://cloud.redhat.com/openshift/install/metal/user-provisioned
    >
    > Download the Command Line Interface for Linux and scp to your VM.

    ``` bash
    tar xf openshift-*-linux.tar.gz
    mv oc bin/
    ```

- ### Ansible

    ``` bash
    sudo dnf install python3
    python3 -m pip install --user ansible
    ```

    - #### Ansible-Operator

        ``` bash
        curl -OJL -OJL https://github.com/operator-framework/operator-sdk/releases/download/v0.8.0/operator-sdk-v0.8.0-x86_64-linux-  gnu
        mv operator-sdk-v0.8.0-x86_64-linux-gnu bin/operator-sdk
        chmod +x bin/operator-sdk
        ```

    - #### Ansible-Runner

        ``` bash
        dnf install python-ansible-runner
        cd /usr/share/ansible/plugins/modules/
        git clone https://github.com/ansible/ansible-runner-http.git
        ```

- ### HELM (In go-task)

    ``` bash
    curl -sL https://get.helm.sh/helm-v3.5.2-linux-amd64.tar.gz | tar xz
    mv linux-amd64/helm linux-amd64/tiller bin
    rm -rf linux-amd64
    ```

- ### Anchore CLI

    ``` bash
    python3 -m pip install --user anchorecli
    ```

- ### Kubernetes

    - #### Kubectl (In go-task)

        ``` bash
        cd temp
        curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
        chmod +x kubectl
        mv kubectl ../bin
        ```

    - #### k9s (In go-task)

        ``` bash
        cd temp
        curl -sL https://github.com/derailed/k9s/releases/download/v0.27.4/k9s_Linux_amd64.tar.gz | tar xz
        mv k9s ../bin/
        rm *
        ```

    - #### kustomize (In go-task)

        ``` bash
        cd  temp
        curl -sL https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv5.1.1/kustomize_v5.1.1_linux_amd64.tar.gz | tar xz
        mv kustomize ../bin/
        ```

    - #### sops (In go-task)

        ``` bash
        cd temp
        curl -LO https://github.com/getsops/sops/releases/download/v3.7.3/sops-v3.7.3.linux.amd64
        chmod +x sops*
        mv sops* ../bin/sops
        ```

- ### ArgoCD CLI (In go-task)

    ``` bash
    cd temp
    wget https://github.com/argoproj/argo-cd/releases/download/v2.0.1/argocd-linux-amd64
    chmod +x argocd-linux-amd64
    mv argocd-linux-amd64 ../bin/argocd
    ```

- ### Shellcheck

    ``` bash
    cd temp
    wget https://github.com/koalaman/shellcheck/releases/download/v0.7.2/shellcheck-v0.7.2.linux.x86_64.tar.xz
    tar xf shellcheck-v0.7.2.linux.x86_64.tar.xz
    cd shellcheck*
    mv shellcheck ../../bin/shellcheck
    ```

- ### Terraform (In go-task)

  ``` bash
  cd temp
  wget https://releases.hashicorp.com/terraform/1.3.4/terraform_1.3.4_linux_amd64.zip
  unzip terraform_1.3.4_linux_amd64.zip
  mv terraform ../bin
  rm terraform_1.3.4_linux_amd64.zip
  ```

- ### Teragrunt (In go-task)

  ``` bash
  cd temp
  wget https://github.com/gruntwork-io/terragrunt/releases/download/v0.38.7/terragrunt_linux_amd64
  chmod +x terragrunt_linux_amd64
  mv terragrunt_linux_amd64 ../bin/terragrunt
  ```

- AWS (In go-task)

  ```bash
  curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
  unzip awscliv2.zip
  sudo ./aws/install
  sudo chmod 755 -R /usr/local/aws-cli
  ```

- RipGrep

  ```bash
  cd temp
  wget https://github.com/BurntSushi/ripgrep/releases/download/13.0.0/ripgrep-13.0.0-x86_64-unknown-linux-musl.tar.gz
  tar xf ripgrep*
  cd ripgrep*
  mv rg ../../bin/
  cd ../
  rm -rf ripgrep*
  ```
