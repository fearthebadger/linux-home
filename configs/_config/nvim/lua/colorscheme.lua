-- define your colorscheme here
local colorscheme = 'OceanicNext'


local is_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not is_ok then
    vim.notify('colorscheme ' .. colorscheme .. ' not found!')
    return

end


-- Set cursor line color
vim.cmd("hi CursorLine ctermfg=none ctermbg=235 cterm=NONE guifg=NONE guibg=none")
vim.cmd("autocmd InsertEnter * hi CursorLine ctermbg=237")
vim.cmd("autocmd InsertLeave * hi CursorLine ctermbg=235")

-- Remove the background color of the main screen
vim.cmd("hi Normal guibg=none ctermbg=none")
vim.cmd("hi EndOfBuffer guibg=none ctermbg=none")

-- Remove the background color of the gutter.
--"g:gitgutter_override_sign_column_highlight = 0")
vim.cmd("hi SignColumn guibg=NONE ctermbg=NONE")
vim.cmd("hi GitGutterAdd guibg=NONE ctermbg=NONE")
vim.cmd("hi GitGutterChange guibg=NONE ctermbg=NONE")
vim.cmd("hi GitGutterDelete guibg=NONE ctermbg=NONE")
vim.cmd("hi GitGutterChangeDelete guibg=NONE ctermbg=NONE")

-- Clear out the LineNo background
vim.cmd("hi LineNr guibg=NONE ctermbg=NONE")

-- Change Color when entering Insert Mode
vim.cmd("hi CursorLineNr cterm=none term=none ctermbg=97 ctermfg=none")
vim.cmd("autocmd InsertEnter * hi  CursorLineNr ctermbg=22 ctermfg=none")
vim.cmd("autocmd InsertLeave * hi  CursorLineNr ctermbg=97 ctermfg=none")

-- Only underline words that are misspelled.
-- But do not underline words it thinks need to be Capitalized.
vim.cmd("hi clear SpellBad")
vim.cmd("hi clear SpellCap")
vim.cmd("hi SpellBad cterm=underline term=underline")
vim.cmd("hi SpellCap cterm=none term=none")
vim.cmd("hi SpellLocal ctermbg=NONE")

-- Overwrite background colors
vim.cmd("hi Search cterm=none ctermbg=88 ctermfg=none")
vim.cmd("hi Comment guifg=#534867 ctermfg=240")
vim.cmd("hi Todo cterm=none ctermbg=none ctermfg=172")

-- This is a way to color the characters in daily
vim.cmd("hi DarkGreyChar   ctermfg=234")
vim.cmd("hi GreyChar       ctermfg=237")
vim.cmd("hi lightGreyChar  ctermfg=277")
vim.cmd("hi greenChar      ctermfg=10")
vim.cmd("hi lightRedChar   ctermfg=132")
vim.cmd("hi redChar        ctermfg=1")
vim.cmd("hi yellowChar     ctermfg=227")
vim.cmd("hi blueChar       ctermfg=68")
vim.cmd("hi orangeChar     ctermfg=215")
vim.cmd("hi pinkChar       ctermfg=203")
vim.cmd("hi purpleChar     ctermfg=99")
vim.cmd("hi cyanChar       ctermfg=81")
vim.fn.matchadd('orangeChar', '2[0-9]\\{3\\}-[0-9]\\{2\\}-[0-9]\\{2\\}') --matches 2020-02-07
vim.fn.matchadd('pinkChar', 'for: 2[0-9]\\{3\\}-[0-9]\\{2\\}-[0-9]\\{2\\}') --matches for: 2020-02-07
vim.fn.matchadd('DarkGreyChar', '│2[0-9]\\{3\\}-[0-9]\\{2\\}-[0-9]\\{2\\}│') --matches │2020-02-07│
--au BufWinEnter *.md call matchadd('cyanChar','#[^ ,.`]\+') "matches #tag-var in markdown files
--au BufWinEnter *.md call matchadd('PurpleChar', '+[^ `]\+') "matches +daily in markdown files
vim.fn.matchadd('blueChar', '□')
vim.fn.matchadd('greenChar', '✓')
vim.fn.matchadd('redChar', '✗')
vim.fn.matchadd('yellowChar', '○')
vim.fn.matchadd('blueChar', '▶')
vim.fn.matchadd('lightRedChar', '?')
vim.fn.matchadd('lightRedChar', '!')
vim.fn.matchadd('lightRedChar', '#? ')
vim.fn.matchadd('lightRedChar', '#! ')

